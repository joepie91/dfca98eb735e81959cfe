(function(){
	window.fetch(window.location).then(function(response) {
		return response.text();
	}).then(function(responseText) {
		var fakeDocument = (new DOMParser()).parseFromString(responseText, "text/html");
		
		if (window.location.toString().match(/^https?:\/\/(?:[^\.]+\.)?v3\.co\.uk[\/$]/)) {
			// v3.co.uk
			var articleContents = fakeDocument.querySelector(".article-page-body-content");

			var prevElement = document.querySelector("#rdm-below-summary");
			prevElement.parentNode.insertBefore(articleContents, prevElement.nextSibling);

			var veilElements = Array.from(document.querySelectorAll(".r3z-wait")).concat(Array.from(document.querySelectorAll(".r3z-hide")));
			veilElements.forEach(function(element) {
				element.parentNode.removeChild(element);
			});
			
		} else if (window.location.toString().match(/^https?:\/\/(?:[^\.]+\.)?wired\.(?:co\.uk|com)[\/$]/)) {
			// Wired
			var articleContents = fakeDocument.querySelector("[data-js='post']");

			var prevElement = document.querySelector("[data-js='postHeader']");
			prevElement.parentNode.insertBefore(articleContents, prevElement.nextSibling);

			var veilElement = document.querySelector("#veil");
			veilElement.parentNode.removeChild(veilElement);

			document.body.classList.remove("no-scroll");
		}
	});
})();