## Supported sites:

* Wired
* V3.co.uk

Requests for other sites are welcome (admin@cryto.net).

## Changelog

* __July 25, 2016:__ Added support for V3.co.uk.
* __February 19, 2016:__ Initial release for Wired.

## Broken?

GitHub Gist doesn't send notifications when people leave a comment, so shoot me an e-mail at admin@cryto.net. I'll gladly fix it. Fuck advertising.